import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  Menu = [];
  constructor(public dataService: DataService, private activatedRoute: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
    this.Menu = this.dataService.Menu;

    const initialFragment = this.activatedRoute.snapshot.fragment;
    if (initialFragment.length > 0) {
      this.navigateTo(initialFragment);
    }

    this.router.events.subscribe(s => {
      if (s instanceof NavigationEnd) {
        const tree = this.router.parseUrl(this.router.url);

        this.navigateTo(tree.fragment);
      }
    });

 }

 navigateTo(fragmentString: string) {
   if (fragmentString) {
     const element = document.getElementById(fragmentString);
     if (element) { element.scrollIntoView(element); }
   }
 }

}
