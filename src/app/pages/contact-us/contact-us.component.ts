import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import {DomSanitizer} from '@angular/platform-browser';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmailService} from '../../services/email.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css'],

})
export class ContactUsComponent implements OnInit {

  contactusForm: FormGroup;
  EmailSent = false;
  ErrorEmailSent = false;
  mapResourceLink = '';
  WaitingMessage = false;
  constructor(public dataService: DataService, public sanitizer: DomSanitizer, private emailService: EmailService) { }

  ngOnInit() {
    this.contactusForm = new FormGroup({
      'Name' : new FormControl(null, Validators.required),
      'Email' : new FormControl(null, [Validators.required, Validators.email]),
      'MobileNo' : new FormControl(null, [Validators.required]),
      'Message' : new FormControl(null, [Validators.required])
    });

    this.mapResourceLink = this.GoogleMapLink();
  }

  GoogleMapLink() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.dataService.CompanyInfo.GoogleAddressLink).toString();
  }

  onSubmit() {
    console.log(this.contactusForm);
    this.emailService.EmailDetails.Name = this.contactusForm.value.Name;
    this.emailService.EmailDetails.email = this.contactusForm.value.Email;
    this.emailService.EmailDetails.mobile = this.contactusForm.value.MobileNo;
    this.emailService.EmailDetails.message = this.contactusForm.value.Message;
    this.EmailSent = false;
    this.ErrorEmailSent = false;
    this.WaitingMessage = true;
    this.emailService.SendEmail()
      .subscribe(
        (response) => {
          console.log(response);
          if (response === 'OK') {
            this.EmailSent = true;
            this.contactusForm.reset();
            this.WaitingMessage = false;
          } else {
            this.EmailSent = false;
            this.ErrorEmailSent = true;
            this.WaitingMessage = false;

          }
        },
        (error) => {
          this.ErrorEmailSent = true;
          this.EmailSent = false;
          this.WaitingMessage = false;
          console.log(error);
        }
      );

  }
}
