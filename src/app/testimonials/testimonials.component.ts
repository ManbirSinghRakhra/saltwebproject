import {AfterViewInit, Component, OnInit} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    /* ******************************************** */
    /*  JS for FlexSlider  */
    /* ******************************************** */

      $('.flexslider-recent').flexslider({
        animation:		'fade',
        animationSpeed:	1000,
        controlNav:		true,
        directionNav:	false
      });
      $('.flexslider-testimonial').flexslider({
        animation: 		'fade',
        slideshowSpeed:	5000,
        animationSpeed:	1000,
        controlNav:		true,
        directionNav:	false
      });
  }

}
