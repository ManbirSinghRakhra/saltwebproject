import {AfterViewInit, Component, OnInit} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit, AfterViewInit {
  state = 'normal';
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    /* ******************************************** */
    /*  JS for SLIDER REVOLUTION  */
    /* ******************************************** */
    $(document).ready(function() {
      $('.tp-banner').revolution(
        {
          delay: 9000,
          startheight: 500,

          hideThumbs: 10,

          navigationType: 'bullet',

          hideArrowsOnMobile: 'on',

          touchenabled: 'on',
          onHoverStop: 'on',

          navOffsetHorizontal: 0,
          navOffsetVertical: 20,

          stopAtSlide: -1,
          stopAfterLoops: -1,

          shadow: 0,

          fullWidth: 'on',
          fullScreen: 'off'
        });
    });
  }
}
