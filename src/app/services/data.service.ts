import {Injectable} from '@angular/core';

@Injectable()
export class DataService {
  CopyrightYear = '2017';
  CompanyInfo = {
      CompanyName: 'The Little Salt',
      WebsiteTitle: 'LittleSalt',
      WebsiteTitleSlogan: 'God is Taste, Taste is God',
      WebsiteTitleLongDescription: 'Litte Salt always welcomes you whole heartedly. You will taste the best meal in the best part of Windsor.',
      AddressLine1: '203/142 Newmarket Rd',
      Suburb: 'Windsor',
      State: 'QLD',
      PostCode: '4030',
      PhoneNo: '(07) 3357 3351',
      Email: 'sales@thelittlesalt.com.au',
      CopyrightYear: this.CopyrightYear,
      GoogleAddressLink: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3541.023189600123!2d153.02170761558287!3d-27.437388021899597!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9159dc8f030d81%3A0x3651334cf20e648f!2sIspa+kebab!5e0!3m2!1sen!2sau!4v1496667123222'
    };


    Menu = [
      {
        CategoryId: 'menu1',
        Category: 'Entree 1',
        Items: [
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          },
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          }
        ]
      },
      {
        CategoryId: 'menu2',
        Category: 'Entree 2',
        Items: [
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          },
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          }
        ]
      },
      {
        CategoryId: 'menu3',
        Category: 'Entree 3',
        Items: [
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          },
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '29'
          }
        ]
      },
      {
        CategoryId: 'menu4',
        Category: 'Entree 4',
        Items: [
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          },
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          }
        ]
      },
      {
        CategoryId: 'menu5',
        Category: 'Entree 5',
        Items: [
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          },
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          }
        ]
      },
      {
        CategoryId: 'menu6',
        Category: 'Entree 6',
        Items: [
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          },
          {
            ItemName: 'Chicken tikka',
            ItemDescription: 'Chicken tikka is a great item. These are its ingredient',
            ItemPrice: '20'
          }
        ]
      }
    ];



  constructor() {

  }

}
