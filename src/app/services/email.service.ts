import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";


@Injectable()
export class EmailService {

  EmailDetails = {
    Name: String,
    email: String,
    mobile: String,
    message: String
  };
  constructor(private http: Http) { }


  SendEmail() {

    //this.http.post('http://localhost:7000/assets/mail/gmail.php', this.EmailDetails);
    const headers = new Headers({'Content-Type': 'application/json'});
    console.log('http://localhost:7000/assets/mail/gmail.php?EmailAddress='+this.EmailDetails.email+'&FullName='+this.EmailDetails.Name+'&MobileNo='+this.EmailDetails.mobile+'&Message='+this.EmailDetails.message );
    return this.http.get('http://10.100.41.138:7000/assets/mail/gmail.php?EmailAddress='+this.EmailDetails.email+'&FullName="'+this.EmailDetails.Name+'"&MobileNo="'+this.EmailDetails.mobile+'"&Message="'+this.EmailDetails.message+'"')
      .map(
        (response: Response) => {
          return response.statusText;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong');
        }
      );

  }

}
