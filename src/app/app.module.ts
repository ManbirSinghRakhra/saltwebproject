import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SliderComponent } from './slider/slider.component';
import {BodyComponent} from './body/body.component';
import {HomeComponent} from './pages/home/home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ShowCaseComponent } from './show-case/show-case.component';
import { NewDishesComponent } from './new-dishes/new-dishes.component';
import { ChefSpecialsComponent } from './chef-specials/chef-specials.component';
import { OfferPriceComponent } from './offer-price/offer-price.component';
import { OurStaffComponent } from './our-staff/our-staff.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { PageBannerComponent } from './page-banner/page-banner.component';
import {RouterModule, Routes} from '@angular/router';
import { ReservationComponent } from './pages/reservation/reservation.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { MenuComponent } from './pages/menu/menu.component';
import { AboutComponent } from './pages/about/about.component';
import {DataService} from './services/data.service';
import {ReactiveFormsModule} from '@angular/forms';
import {EmailService} from './services/email.service';
import {HttpModule} from "@angular/http";

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'contactus', component: ContactUsComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'menu', component: MenuComponent},
  {path: 'about', component: AboutComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    BodyComponent,
    HomeComponent,
    ShowCaseComponent,
    NewDishesComponent,
    ChefSpecialsComponent,
    OfferPriceComponent,
    OurStaffComponent,
    TestimonialsComponent,
    ContactUsComponent,
    PageBannerComponent,
    ReservationComponent,
    GalleryComponent,
    MenuComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  providers: [DataService, EmailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
