<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */
   header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: X-Requested-With');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require './PHPMailer-master/PHPMailerAutoload.php';

$SenderEmailAddress = $_REQUEST["EmailAddress"];
$SenderFullName = $_REQUEST["FullName"];
$SenderMobileNo = $_REQUEST["MobileNo"];
$SenderMessage = $_REQUEST["Message"];


//Server Variable
$FromEmailAddress = "Manbir.Rakhra@gmail.com";
$FromName = "Message from Customer: ".$SenderFullName;
$ReplyEmailAddress = $FromEmailAddress;
$ReplyName = $FromName;

$Subject = "Auto Message";

if(!isset($SenderEmailAddress)&&empty($SenderEmailAddress))
{
    echo "Email Address is not valid";
    return;
}

if(!isset($SenderFullName)&&empty($SenderFullName))
{
    //echo "Name is not valid";
    $SenderFullName = "Hi There";
    //return;
}

if(!isset($SenderMobileNo)&&empty($SenderMobileNo))
{
    echo "Mobile No is not valid";
    return;
}

if(!isset($SenderMessage)&&empty($SenderMessage))
{
    echo "Message is not valid";
    return;
}

$SenderMessage =  "Contact Name: ".$SenderFullName."<br/>Contact Email: ".$SenderEmailAddress."<br/>Contact Mobile No: ".$SenderMobileNo."<br/>Message:".$SenderMessage;

//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "Manbir.Rakhra@gmail.com";

//Password to use for SMTP authentication
$mail->Password = "ManbirSmtp";

//Set who the message is to be sent from
$mail->setFrom($FromEmailAddress, $FromName);

//Set an alternative reply-to address
$mail->addReplyTo($ReplyEmailAddress, $ReplyName);

//Set who the message is to be sent to
$StaffEmailAddress = "manbir.s.rakhra@gmail.com";
$mail->addAddress($StaffEmailAddress, $SenderFullName);

//Set the subject line
$mail->Subject = $Subject;

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->Body = $SenderMessage;

//Replace the plain text body with one created manually
$mail->AltBody = $SenderMessage;

//Attach an image file
//$mail->addAttachment('img/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    //echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo json_encode("Message sent!");
}
