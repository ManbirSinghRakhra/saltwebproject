import { SaltWebSitePage } from './app.po';

describe('salt-web-site App', () => {
  let page: SaltWebSitePage;

  beforeEach(() => {
    page = new SaltWebSitePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
